package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodos() {
        Iterable<Produto> produtos = produtoRepository.findAll();
        return produtos;
    }

    public Produto salvaProduto(Produto produto) {
        Produto produtoObjeto = produtoRepository.save(produto);
        return produtoObjeto;
    }

    public Iterable<Produto> buscarTodosPorNome(String nome) {
        Iterable<Produto> produtos = produtoRepository.findAllByNome(nome);
        return produtos;
    }

    public Produto buscarPorId(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        throw new RuntimeException("Não existe produto cadastrado para o id informado");
    }

    public Produto atualizarProduto(int id, Produto produto) {
        if (produtoRepository.existsById(id)) {
            produto.setId(id);
            Produto produtoObjeto = salvaProduto(produto);
            return produtoObjeto;
        }
        throw new RuntimeException("Não existe produto cadastrado para o id informado");
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Não existe produto cadastrado para o id informado");
        }
    }
}
